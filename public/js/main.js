import {test} from "./test.mjs";
window.options = {
  "customizedHeaderId": true,
  "metadata": true,
  "parseImgDimensions": true,
  "strikethrough": true,
  "tables": true,
  "ghCodeBlocks": true,
  "tasklists": true,
  "encodeEmails": true,
  "underline": true
};
window.converter = new showdown.Converter(window.options);
fetch("./content/test.md").then(resp =>{
  if(resp.ok == false){
    // 404 page here.
  }
  return resp.text();
}).then( content => {
  window.text = content;
  window.html = window.converter.makeHtml(window.text);
  window.metadata = jsyaml.load(window.converter.getMetadata(true)); // returns an object with the document metadata',

  console.log(window.html);
  document.getElementById('html').textContent = window.html;
  console.log(JSON.stringify(window.metadata, null, 2));
  document.getElementById('metadata').textContent = JSON.stringify(window.metadata, null, 2);
  console.log(window.converter.getOption("metadata"));
}
);
